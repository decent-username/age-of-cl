(in-package "AGE-OF-CL")


(defclass button ()
  ((pos
    :accessor pos
    :initform nil)
   (hover-area
    :accessor hover-area
    :initarg :hover-area
    :initform nil
    :documentation "rectangle defined by two gamekit:vec2")
   (press-area
    :accessor press-area
    :initarg :press-area
    :initform nil
    :documentation "rectangle defined by two gamekit:vec2")
   (state
    :accessor state
    :initarg :state
    :initform :released
    :documentation "Button state, one of :released :pressed :hover")
   (signal-ready-p
    :accessor signal-ready-p
    :initform nil
    :documentation "Is non-nil when button was successfully clicked.")
   (func-form
    :accessor func-form
    :initform nil)
   (width
    :accessor width
    :initform nil
    :documentation "Width of the press area.")
   (height
    :accessor height
    :initform nil
    :documentation "Height of the press area.")
   (color
    :accessor color
    :initarg :color
    :initform (gamekit:vec4 0.5 0.5 0.5 1))
   ))

(defclass mode-switch-button (button)
  ((switch-to-mode
    :accessor switch-to-mode
    :initarg :switch-to-mode
    :initform nil
    :documentation "This is a button that is used to switch modes.")
   ))

;;;------------------------------------------------------------
(defmethod initialize-instance :after ((b button)
                                       &key)

  (with-accessors ((ha hover-area)
                   (ca press-area))
      b
    (when (and (not (null ha))
               (not (null ca)))
      (let* ((blc (car ca)) ;; bottom left corner
             (trc (cdr ca)) ;; top right corner
             (width (- (gamekit:x trc) (gamekit:x blc)))
             (height (- (gamekit:y trc) (gamekit:y blc))))
        (setf (pos b) blc)
        (setf (width b) width)
        (setf (height b) height)))))

(defmethod initialize-instance :after ((msb mode-switch-button)
                                       &key)
  )

;;;------------------------------------------------------------
(defun add-button (button mode game-state-obj)
  "Create button, which from bottom left corner to top right corner"
  (with-accessors ((m mode)
                   (all-b-alist all-button-alists)
                   (active-buttons active-buttons))
      game-state-obj
    ;; add button to correct button-list
    (let ((new-b-list (push button (cdr (or (assoc mode all-b-alist)
                                            (error "Missing mode in alist"))))))
      (when (and (eq m mode))
        (setf active-buttons new-b-list)))))


;;;------------------------------------------------------------
(defun update-button-state (button-list)
  "Takes a button list and updates all buttons"
  (dolist (b button-list)
    (case *mouse-state*
      (:left-pressed   (update-button-pressed-state b))
      (:left-released  (update-button-released-state b))
      ;; (:right-pressed  (update-button-pressed-state b))
      ;; (:right-released (update-button-released-state b))
      )
    ))

;;;------------------------------------------------------------
(defun update-button-hover-state (b-list)
  (dolist (b b-list)
    (cond ((and (not (button-hovered-p b))
                (not (eql (state b) :pressed)))
           ;; if the button isn't even hovered, it can only be :released
           (setf (state b) :released))
          ;; at this point we know that this button is at least being hovered
          ((eql *mouse-state* :left-released)
           (setf (state b) :hovered))
          ;; if we get to this point, we know that the mouse button is pressed
          ((cursor-inside-press-area-p b)
           (setf (state b) :pressed))
          ;; if we get to this point, we know that the cursor is not in the click area
          (T (setf (state b) :hovered)))))

;;;------------------------------------------------------------
(defun update-button-pressed-state (b)
  (when (cursor-inside-press-area-p b) ;; this  is only called when the user clicks
    (setf (state b) :pressed)))

;;;------------------------------------------------------------
(defun update-button-released-state (b)
  (when (and (eq (state b) :pressed)
             (cursor-inside-press-area-p b))
    (setf (signal-ready-p b) T)
    (setf (state b) :released)))


;;;------------------------------------------------------------
(defun button-hovered-p (button)
  (let* ((hover-area (hover-area button))
         (mouse-pos-x (gamekit:x *mouse-pos*))
         (mouse-pos-y (gamekit:y *mouse-pos*))
         (blc   (car hover-area))
         (blc-x (gamekit:x blc))
         (blc-y (gamekit:y blc))
         (trc   (cdr hover-area))
         (trc-x (gamekit:x trc))
         (trc-y (gamekit:y trc)))

      (and (and (<= mouse-pos-x trc-x)
                (>= mouse-pos-x blc-x))
           (and (<= mouse-pos-y trc-y)
                (>= mouse-pos-y blc-y)))))

;;;------------------------------------------------------------
(defun cursor-inside-press-area-p (button)
  (let* ((press-area (press-area button))
         (mouse-pos-x (gamekit:x *mouse-pos*))
         (mouse-pos-y (gamekit:y *mouse-pos*))
         (blc   (car press-area))
         (blc-x (gamekit:x blc))
         (blc-y (gamekit:y blc))
         (trc   (cdr press-area))
         (trc-x (gamekit:x trc))
         (trc-y (gamekit:y trc)))

    (and (and (<= mouse-pos-x trc-x)
              (>= mouse-pos-x blc-x))
         (and (<= mouse-pos-y trc-y)
              (>= mouse-pos-y blc-y)))))

;;;------------------------------------------------------------
(defgeneric exec-button-action (b))

(defmethod exec-button-action ((b mode-switch-button))
  (change-game-mode *main-game-object* (switch-to-mode b)))

;;;------------------------------------------------------------
