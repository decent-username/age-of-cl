(in-package "AGE-OF-CL")


;;;------------------------------------------------------------------------------
;;; Exported Functions
;;;------------------------------------------------------------------------------
(defun start ()
  (gamekit:start 'age-of-cl))

(defun stop ()
  (gamekit:stop))


(defun reset ()
  (sb-ext:gc :full t)
  (setf *main-game-object* (make-instance 'game-state :mode (mode *main-game-object*)))
  (make-menu-bar :list-of-modes '(:testing :splash :ingame)
                 :origin (gamekit:add *win-top-left-corner*
                                      (gamekit:vec2 0 -25))
                 :area-width 400
                 :area-height 25
                 :color (gamekit:vec4 .3 .5 .6 1)
                 :game-state *main-game-object*)
  (setf (active-map *main-game-object*)
        (make-instance 'basic-map
                       :basic-map-width  *world-width*
                       :basic-map-height *world-height*)))


;;;------------------------------------------------------------------------------
;;; Utility Functions
;;;------------------------------------------------------------------------------
(defun real-time-seconds ()
  "Return seconds since certain point of time"
  (/ (get-internal-real-time) internal-time-units-per-second))



;;;------------------------------------------------------------------------------
;;; Defining Global Variables/Constants
;;;------------------------------------------------------------------------------
;;; Canvas Parameters
;; 1 means 256x144
(defparameter *window-ratio* (cons 16 9))
(defparameter *win-scale-factor* 4)

(defparameter *tile-size* 8)

(defparameter *h-tile-count* (* (car *window-ratio*) (/ 16 *tile-size*)))
(defparameter *v-tile-count* (* (cdr *window-ratio*) (/ 16 *tile-size*)))




;;; dimensions of the window
;;; don't use these values for game logic
;;; the game logic is fixed at 256x144
(defparameter *window-width*  (* *win-scale-factor* (car *window-ratio*)  16))
(defparameter *window-height* (* *win-scale-factor* (cdr *window-ratio*) 16))

(defparameter *window-dimensions* (cons *window-width* *window-height*))

;;; Position Constants on Window
(defparameter *win-origin*              (gamekit:vec2 0 0))
(defparameter *win-bottom-left-corner*  (gamekit:vec2 0 0))
(defparameter *win-top-left-corner*     (gamekit:vec2 0 *window-height*))
(defparameter *win-bottom-right-corner* (gamekit:vec2 *window-height* 0))
(defparameter *win-top-right-corner*    (gamekit:vec2 *window-width* *window-height*))
(defparameter *win-bottom-middle*           (gamekit:vec2 (/ *window-width* 2) 0))
(defparameter *win-center*              (gamekit:vec2 (/ *window-width* 2)
                                                      (/ *window-height* 2)))

;;; Game related Variables/Constants

(defparameter *canvas-width* 256)
(defparameter *canvas-height* 144)
(defparameter *canvas-dimensions* (cons *canvas-width* *canvas-height*))

;;; Position Constants
(defparameter *origin*              (gamekit:vec2 0 0))
(defparameter *bottom-left-corner*  (gamekit:vec2 0 0))
(defparameter *top-left-corner*     (gamekit:vec2 0 *canvas-height*))
(defparameter *bottom-right-corner* (gamekit:vec2 *canvas-height* 0))
(defparameter *top-right-corner*    (gamekit:vec2 *canvas-width* *canvas-height*))
(defparameter *center*              (gamekit:vec2 (/ *canvas-width* 2)
                                                  (/ *canvas-height* 2)))


;;; Mouse
(defparameter *mouse-pos* *center*)
(defparameter *mouse-state* :released)


;;; Colors Constants
;;; Color Vectors are always of type gamekit:vec4
(defvar +black+ (gamekit:vec4 0 0 0 1))
(defvar +white+ (gamekit:vec4 1 1 1 1))
(defvar +red+   (gamekit:vec4 1 0 0 1))
(defvar +green+ (gamekit:vec4 0 1 0 1))
(defvar +blue+  (gamekit:vec4 0 0 1 1))

;;; Other Variables
(defparameter *world-width* 256)
(defparameter *world-height* 256)
(defparameter *world-dimensions* (cons *world-width* *world-height*))

(defparameter *update-map-p* nil)
(defparameter *update-tick* (real-time-seconds))
;;;------------------------------------------------------------------------------
;;; Define Game
;;;------------------------------------------------------------------------------
(gamekit:defgame age-of-cl () ()
                 (:viewport-width *window-width* )
                 (:viewport-height *window-height*)
                 (:viewport-title "age-of-cl")
                 (:prepare-resources t))

;;;------------------------------------------------------------------------------
;;; Defining Global Resources
;;;------------------------------------------------------------------------------
(gamekit:register-resource-package
 :keyword (asdf:system-relative-pathname :age-of-cl "assets/"))

;;; Images
(gamekit:define-image :img-splash-screen "img/splash-1.png")

;;; Sounds
(gamekit:define-sound :snd-mushroom-dance-loop "snd/music-loop/mushroom-dance-loop.ogg")
(gamekit:define-sound :snd-music-jewels-loop "snd/music-loop/music-jewels-loop.ogg")
(gamekit:define-sound :snd-underwater-loop "snd/music-loop/underwater-loop.wav")
(gamekit:define-sound :snd-mouse-click-1 "snd/UI-SFX/mouseclick1.wav")
(gamekit:define-sound :snd-mouse-release-1 "snd/UI-SFX/mouserelease1.wav")


;;; Fonts
(gamekit:define-font :font-fira-mono-bold "font/FiraMono-Bold.ttf")
(gamekit:define-font :font-fira-mono-medium "font/FiraMono-Medium.ttf")
(gamekit:define-font :font-fira-mono-regular "font/FiraMono-Regular.ttf")

;;;------------------------------------------------------------------------------
;;; Post Init
;;;------------------------------------------------------------------------------
(defmethod gamekit:post-initialize ((app age-of-cl))
  (gamekit:bind-cursor (lambda (x y)
                         (setf (gamekit:x *mouse-pos*) x)
                         (setf (gamekit:y *mouse-pos*) y)
                         (update-button-hover-state (active-buttons *main-game-object*))
                         (update-button-hover-state (menu-bar *main-game-object*))
                         ))

  (gamekit:bind-button :mouse-left :pressed
                       (lambda ()
                         (gamekit:play-sound :snd-mouse-click-1)
                         (setf *mouse-state* :left-pressed)
                         ))

  (gamekit:bind-button :mouse-left :released
                       (lambda ()
                         (gamekit:play-sound :snd-mouse-release-1)
                         (setf *mouse-state* :left-released)
                         ))

  (gamekit:bind-button :mouse-right :pressed
                       (lambda ()
                         (gamekit:play-sound :snd-mouse-click-1)
                         (setf *mouse-state* :right-pressed)
                         ))

  (gamekit:bind-button :mouse-right :released
                       (lambda ()
                         (gamekit:play-sound :snd-mouse-release-1)
                         (setf *mouse-state* :right-released)
                         ))
  )

;;;------------------------------------------------------------------------------
;;; Pre Destroy
;;;------------------------------------------------------------------------------
(defmethod gamekit:pre-destroy ((app age-of-cl))
  (trivial-garbage:gc :full t)
  (sb-ext:gc :full t)) ; this might not be portable to other Common Lisp implementations.

#|
This is a multi-line comment.
|#



;;;------------------------------------------------------------------------------
;;; Define Game-Object
;;;------------------------------------------------------------------------------
(defclass game-state ()
  ((mode
    :accessor mode
    :initarg :mode
    :initform :splash)
   (all-musics
    :accessor all-musics
    :initarg :all-musics
    :initform (list (cons :testing nil)
                    (cons :splash nil)
                    (cons :splash-menu nil)
                    (cons :ingame nil)
                    (cons :ingame-pause nil)))
   (active-music
    :accessor active-music
    :initform nil)
   (menu-bar
    :accessor menu-bar
    :initarg :menu-bar
    :initform nil)
   (all-button-alists
    :accessor all-button-alists
    :initarg :all-button-alist
    :initform (list (cons :testing nil)
                    (cons :splash nil)
                    (cons :splash-menu nil)
                    (cons :ingame nil)
                    (cons :ingame-pause nil))
    :documentation "Association list containing keyword and list of buttons.")
   (active-buttons
    :accessor active-buttons
    :initarg :active-buttons
    :initform nil)
   (active-map
    :accessor active-map
    :initarg :active-map
    :initform nil)))


(defmethod initialize-instance :after ((game-obj game-state) &key)
  (with-accessors ((all-button-alists all-button-alists)
                   (active-buttons active-buttons)
                   (active-music active-music)
                   (all-musics all-musics)
                   (mode mode))
      game-obj
    (setf active-buttons (cdr (assoc mode all-button-alists))
          active-music   (cdr (assoc mode all-musics)))))


(defgeneric draw-game-object (x))

(defmethod draw-game-object ((game-obj game-state))
  (ecase (mode game-obj)
    ;;------------------------------------------------------------
    (:testing
     (gamekit:draw-rect *origin*
                        *canvas-width*
                        *canvas-height*
                        :fill-paint +black+)
     (draw-grid)
     (draw-tiles '((0 . 0) (0 . 1))
                            :fill-paint (gamekit:vec3 0.3 0.4 0.5))
     )
    ;;------------------------------------------------------------
    (:splash
     (gamekit:draw-image *origin*
                         :img-splash-screen)

     )
    ;;------------------------------------------------------------
    (:splash-settings
     )
    ;;------------------------------------------------------------
    (:ingame
     (gamekit:draw-rect *origin*
                        *canvas-width*
                        *canvas-height*
                        :fill-paint *water-color*)
     ;; (draw-grid)
     (draw-map (active-map *main-game-object*))


     )
    ;;------------------------------------------------------------
    (:ingame-pause
     )))


(defgeneric act-game-object (x))

(defmethod act-game-object ((game-obj game-state))
  ;; first we need to process
  (with-accessors ((active-b active-buttons)
                   (menu-bar menu-bar))
      *main-game-object*

    (update-button-state menu-bar)
    (update-button-state active-b)

    (dolist (b active-b)
      (when (signal-ready-p b)
        (format t "~&Signal from: ~S.~%" b)
        (exec-button-action b)
        (setf (signal-ready-p b) nil)))

    (dolist (b menu-bar)
      (when (signal-ready-p b)
        (format t "~&Signal from: ~S.~%" b)
        (exec-button-action b)
        (setf (signal-ready-p b) nil)))

    )

  (ecase (mode game-obj)
    ;;------------------------------------------------------------
    (:testing

     )
    ;;------------------------------------------------------------
    (:splash
     (when (null (active-music *main-game-object*))
       (setf (active-music game-obj) :snd-underwater-loop)
       (gamekit:play-sound (active-music game-obj) :looped-p T))
     )
    ;;------------------------------------------------------------
    (:splash-settings
     )
    ;;------------------------------------------------------------
    (:ingame


     )
    ;;------------------------------------------------------------
    (:ingame-pause

     )))


(defgeneric change-game-mode (obj m))

(defmethod change-game-mode ((game-obj game-state) m)
  (with-accessors ((active-music active-music)
                   (all-button-alists all-button-alists)
                   (active-buttons active-buttons)
                   (all-musics all-musics)
                   (mode mode))
      game-obj
    (when (not (null active-music)) ; stop music if playing
      (gamekit:stop-sound active-music))
    (setf mode m
          active-buttons (cdr (assoc mode all-button-alists))
          active-music (cdr (assoc mode all-musics)))))

;;; Our Main Game Object
(defparameter *main-game-object* (make-instance 'game-state :mode :testing))


;;;------------------------------------------------------------------------------
;;; Every Frame Calculations
;;;------------------------------------------------------------------------------
(defmethod gamekit:act ((app age-of-cl))
  (act-game-object *main-game-object*)

  )

;;;------------------------------------------------------------------------------
;;; Every Frame Drawing
;;:------------------------------------------------------------------------------
(defmethod gamekit:draw ((app age-of-cl))
  (gamekit:with-pushed-canvas ()
    (gamekit:scale-canvas *win-scale-factor* *win-scale-factor*)

    (draw-game-object *main-game-object*)
    )

  ;; outside scaling by *win-scale-factor*
  (draw-mode-info (symbol-name (mode *main-game-object*))
                             :origin *win-bottom-left-corner*
                             :width 200
                             :height 100)
  (draw-buttons (active-buttons *main-game-object*))
  (draw-buttons (menu-bar *main-game-object*)) ; draw menu-bar buttons

  )


;;;------------------------------------------------------------------------------
;;; Testing
;;;------------------------------------------------------------------------------
(defun make-mode-switch-buttons-list (list-of-modes
                                      &key origin area-width area-height color)
  "This function returns a list of mode-switch-buttons, dependent on the input.

`area-width' defines width of drawing area in which all buttons are put
`area-height' defines height of drawing area in which all buttons are put"
  (let* ((mode-list-length (length list-of-modes))
         (num-gaps (1- mode-list-length))
         (b-width (/ area-width (1+ mode-list-length)))
         (gap-width (/ b-width num-gaps)) ; total width of all gaps is width of one button
         (b-height area-height)
         (result nil) ; list of buttons returned at the end
         )
    (dotimes (i mode-list-length)
      (let* ((curr-mode (nth i list-of-modes))
             (x-offset (* i (+ b-width gap-width)))
            (blc (gamekit:add origin
                              (gamekit:vec2 x-offset 0)))
            (trc (gamekit:add origin
                              (gamekit:vec2 (+ x-offset b-width) b-height))))
        (push (make-instance 'mode-switch-button
                             :color color
                             :switch-to-mode curr-mode
                             :hover-area (cons blc trc)
                             :press-area (cons blc trc))
              result)))
    result))

(defun make-menu-bar (&key list-of-modes origin area-width area-height color
                        game-state)
  (setf (menu-bar game-state)
        (make-mode-switch-buttons-list list-of-modes
                                       :origin origin
                                       :color color
                                       :area-width area-width
                                       :area-height area-height)))


;;; sample call to make-menu-bar
(make-menu-bar :list-of-modes '(:testing :splash :ingame)
               :origin (gamekit:add *win-top-left-corner*
                                    (gamekit:vec2 0 -25))
               :area-width 400
               :area-height 25
               :color (gamekit:vec4 .3 .5 .6 1)
               :game-state *main-game-object*)



;; ;;; sample call to make-mode-switch-buttons-list
;; (make-mode-switch-buttons-list '(:testing :splash :ingame)
;;			       :origin *win-bottom-left-corner*
;;			       :area-width 200
;;			       :area-height 50)


;;;------------------------------------------------------------
;;; Add Stuff
;;;------------------------------------------------------------

(defun add-buttons (game-state mode count &key origin)
  (let* ((y-num (cdr *window-ratio*))
         (offset (/ *window-height* y-num))
         (mode :testing))
    (dotimes (i (- y-num 4))
      (let ((blc (gamekit:add *win-bottom-middle*
                              (gamekit:vec2 0 (* i offset))))
            (trc (gamekit:add *win-bottom-middle*
                              (gamekit:vec2 200 (+ (* i offset) (- offset 10))))))
        (add-button
         (make-instance 'mode-switch-button
                        :color (gamekit:vec3 0.2 0.4 0.6)
                        :switch-to-mode :splash
                        :hover-area (cons blc trc)
                        :press-area (cons blc trc))
         mode
         game-state)))))

(let* ((y-num (cdr *window-ratio*))
       (offset (/ *window-height* y-num))
       (mode :splash))
  (dotimes (i (- y-num 4))
    (let ((blc (gamekit:add *win-bottom-middle*
                            (gamekit:vec2 0 (* i offset))))
          (trc (gamekit:add *win-bottom-middle*
                            (gamekit:vec2 200 (+ (* i offset) (- offset 10))))))
      (add-button
       (make-instance 'mode-switch-button
                      :color (gamekit:vec3 0.2 0.4 0.6)
                      :switch-to-mode :testing
                      :hover-area (cons blc trc)
                      :press-area (cons blc trc))
       mode
       *main-game-object*))))

(setf (active-map *main-game-object*)
      (make-instance 'basic-map
                     :basic-map-width  *world-width*
                     :basic-map-height *world-height*))
