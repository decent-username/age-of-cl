(defpackage "AGE-OF-CL"
  (:nicknames "AOCL")
  (:use "COMMON-LISP")
  (:export "START"
           "STOP"
           "RESET"))
