(in-package "AGE-OF-CL")


(defclass basic-map ()
  ((basic-map-width
    :accessor basic-map-width
    :initarg :basic-map-width
    :initform (error "You need to specify basic-map-width."))
   (basic-map-height
    :accessor basic-map-height
    :initarg :basic-map-height
    :initform (error "You need to specify basic-map-height."))
   (basic-map-tiles
    :accessor basic-map-tiles
    :initarg :basic-map-tiles
    :initform nil)))


(defmethod initialize-instance :after ((m basic-map) &key)
  (with-accessors ((w basic-map-width)
                   (h basic-map-height)
                   (tiles basic-map-tiles))
      m
    (setf tiles (generate-map w h :num-steps 4
                              :no-gaps t))
    (map-clean tiles)))


(defparameter *birth-limit* 2)
(defparameter *death-limit* 7)


(defun generate-map (w h &key (num-steps 5)
                           (no-gaps t))
  (let ((cell-map-arr (make-array (list w h) :initial-element nil))
        (chance-to-start-alive 0.08) ; the lower the fewer tiles
        (num-of-steps num-steps)) ; the higher the smoother
    ;; initialize result-map with random values
    (dotimes (i w)
      (dotimes (j h)
        (if (< (random 1.0) chance-to-start-alive)
            (setf (aref cell-map-arr i j) T))))

    (dotimes (i num-of-steps)
      (setf cell-map-arr (do-simulation-step cell-map-arr :no-gaps no-gaps)))

    cell-map-arr))



(defun do-simulation-step (old-map-arr &key no-gaps)
  (let ((new-map-arr (make-array (array-dimensions old-map-arr) :initial-element nil))
        (map-x-dim (array-dimension old-map-arr 0))
        (map-y-dim (array-dimension old-map-arr 1)))
    ;; Loop over each row and column of the map
    (dotimes (x map-x-dim)
      (dotimes (y map-y-dim)
        (let ((nbs (count-alive-neighbours old-map-arr x y)))
          ;; The new value is based on our simulation rules
          ;; First, if a cell is alive but has too few neighbours, kill it.
          (if (aref old-map-arr x y)
              (if (> nbs *death-limit*)
                  (if no-gaps
                      (setf (aref new-map-arr x y) t)
                      (setf (aref new-map-arr x y) nil)) ; nil
                  (setf (aref new-map-arr x y) T)) ; t
              (if (< nbs *birth-limit*)
                  (setf (aref new-map-arr x y) nil) ; nil
                  (setf (aref new-map-arr x y) T)))))) ;t
        new-map-arr))

;;----------------------------------------------------------------------
(defun count-alive-neighbours (map-arr x y)
  (let ((map-x-dim (array-dimension map-arr 0))
        (map-y-dim (array-dimension map-arr 1))
        (count 0))
   (do ((i -1 (1+ i)))
       ((= i 2) 'finished-i)
     (do ((j -1 (1+ j)))
         ((= j 2) 'finished-j)
       (let ((neighbour-x (+ x i))
             (neighbour-y (+ y j)))
         (cond
           ;; If we're looking at the middle
           ((and (zerop i) (zerop j))
            nil)
           ;; In case the index we're looking off the edge
           ((or (< neighbour-x 0)
                (< neighbour-y 0)
                (>= neighbour-x map-x-dim)
                (>= neighbour-y map-y-dim))
            ;; (incf count) ;; if you enable this, bottom and left edge behave weirdly
            nil
            )
           ((aref map-arr neighbour-x neighbour-y)
            (incf count))))))
   count))


;;;------------------------------------------------------------
(defgeneric map-remove-isolated (m))

(defmethod map-remove-isolated ((m basic-map))
  (with-accessors ((w basic-map-width)
                   (h basic-map-height)
                   (map-arr basic-map-tiles))
        m
      (dotimes (x w)
        (dotimes (y h)
          ;; The new value is based on our simulation rules
          ;; First, if a cell is alive but has too few neighbours, kill it.
          (if (aref map-arr x y)
              (let  ((nbs (count-alive-neighbours map-arr x y)))
                (when (or (= nbs 1)
                          (zerop nbs))
                  (setf (aref map-arr x y) nil))))))))


(defmethod map-remove-isolated ((map-arr array))
  (let ((w (array-dimension map-arr 0))
        (h (array-dimension map-arr 1)))
    (dotimes (x w)
      (dotimes (y h)
        ;; The new value is based on our simulation rules
        ;; First, if a cell is alive but has too few neighbours, kill it.
        (if (aref map-arr x y)
            (let  ((nbs (count-alive-neighbours map-arr x y)))
              (when (or (= nbs 1)
                        (zerop nbs))
                (setf (aref map-arr x y) nil)))))))
  )

;;;------------------------------------------------------------
(defgeneric map-fill-surrounded (m))

(defmethod map-fill-surrounded ((m basic-map))
  (with-accessors ((w basic-map-width)
                   (h basic-map-height)
                   (map-arr basic-map-tiles))
      m
      (dotimes (x w)
        (dotimes (y h)
          ;; The new value is based on our simulation rules
          ;; First, if a cell is alive but has too few neighbours, kill it.
          (unless (aref map-arr x y)
            (let  ((nbs (count-alive-neighbours map-arr x y)))
              (when (= nbs 8)
                (setf (aref map-arr x y) t))))))))


(defmethod map-fill-surrounded ((map-arr array))
  (let ((w (array-dimension map-arr 0))
        (h (array-dimension map-arr 1)))
    (dotimes (x w)
      (dotimes (y h)
        ;; The new value is based on our simulation rules
        ;; First, if a cell is alive but has too few neighbours, kill it.
        (unless (aref map-arr x y)
          (let  ((nbs (count-alive-neighbours map-arr x y)))
            (when (= nbs 8)
              (setf (aref map-arr x y) t))))))))

;;----------------------------------------------------------------------
;; (defgeneric map-remove-trapped-water (m))

;; (defmethod map-remove-trapped-water ((m basic-map))
;;   (with-accessors ((w basic-map-width)
;;                    (h basic-map-height)
;;                    (map-arr basic-map-tiles))
;;                   m
;;                   (dotimes (x w)
;;                     (dotimes (y h)
;;                       ;; The new value is based on our simulation rules
;;                       ;; First, if a cell is alive but has too few neighbours, kill it.
;;                       (unless (aref map-arr x y)
;;                         (let  ((nbs (count-alive-neighbours map-arr x y)))
;;                           (when (= nbs 2)
;;                             (setf (aref map-arr x y) t))))))))

;; (defmethod map-remove-trapped-water ((map-arr array))
;;   (let ((w (array-dimension map-arr 0))
;;         (h (array-dimension map-arr 1)))

;;     (dotimes (x w)
;;       (dotimes (y h)
;;         ;; The new value is based on our simulation rules
;;         ;; First, if a cell is alive but has too few neighbours, kill it.
;;         (unless (aref map-arr x y)
;;           (let  ((nbs (count-alive-neighbours map-arr x y)))
;;             (when (= nbs 2)
;;               (setf (aref map-arr x y) t))))))))


;;----------------------------------------------------------------------
(defun map-clean (tiles)
  (map-fill-surrounded tiles)
  (map-remove-isolated tiles)
  ;; (map-remove-trapped-water tiles)
  )
