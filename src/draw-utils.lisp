(in-package "AGE-OF-CL")

;;;----------------------------------------------------------------------
(defun grey-shade (&key (value 0)  (opacity 1))
  "Value and opacity need to be between 0 and 1."
  (gamekit:vec4 value value value opacity))

;;;----------------------------------------------------------------------
(defun draw-rect-from-center (origin w h
                                     &key (fill-paint nil)
                                     (stroke-paint nil)
                                     (thickness 1.0)
                                     (rounding 0.0))
  (gamekit:draw-rect (gamekit:subt origin (gamekit:vec2 w h))
                     (1+ (* w 2)) ;; the 1+ fixes the vertical stripes
                     (* h 2)
                     :fill-paint   fill-paint
                     :stroke-paint stroke-paint
                     :thickness    thickness
                     :rounding     rounding))
;;----------------------------------------------------------------------
(defun calc-real-pos (pos)
  "`pos' is a component of a position vector."
  (let ((tile-size *tile-size*))
    (+ (* pos tile-size) (/ tile-size 2))))

;;;----------------------------------------------------------------------
(defun draw-tile (point &key fill-paint
                        (stroke-paint fill-paint)
                        (thickness    (/ *tile-size* 8))
                        (rounding     0))
  "`point' is a cons"
  (labels ()
    (let ((x-pos (calc-real-pos (car point)))
          (y-pos (calc-real-pos (cdr point)))
          (tile-size *tile-size*))

      (draw-rect-from-center (gamekit:vec2  x-pos y-pos)
                             (- (/ tile-size 2) (/ thickness 2))
                             (- (/ tile-size 2) (/ thickness 2))
                             :fill-paint   fill-paint
                             :stroke-paint stroke-paint
                             :thickness    thickness
                             :rounding     rounding))))

;;;----------------------------------------------------------------------
(defun draw-tiles (points &key fill-paint
                          (stroke-paint fill-paint)
                          (thickness    (/ *tile-size* 8))
                          (rounding     0))
  "`points' is a list containing points to draw
   `color'"
  (dolist (p points)
    (draw-tile p
               :fill-paint fill-paint
               :thickness 0)))

;;;----------------------------------------------------------------------
(defun draw-grid ()
  (let ((tile-size *tile-size*))
    (labels ((draw-tile-marker (pos)
               "draw dots that designate a tile"
               (gamekit:draw-rect pos
                                  tile-size tile-size
                                  :fill-paint nil
                                  :stroke-paint (grey-shade :value .1)
                                  :thickness 1)))
      (let* ((w *window-width*)
             (h *window-height*)
             (w-times (/ w tile-size))
             (h-times (/ h tile-size)))
        (do ((w-idx 0 (1+ w-idx))
             (h-idx 0))
            ((>= h-idx h-times) T)
          (when (>= w-idx w-times)
            (setf w-idx (mod w-idx w-times))
            (incf h-idx))
          (let ((x-pos (* w-idx tile-size))
                (y-pos (* h-idx tile-size)))
            (draw-tile-marker (gamekit:vec2 x-pos y-pos))))))))

;;;----------------------------------------------------------------------
(defun draw-mode-info (mode-string &key origin width height)
  (let ((font-size (/ height 3)))
    (gamekit:draw-text mode-string
                       origin
                       :fill-color (grey-shade :value 1)
                       :font (gamekit:make-font :font-fira-mono-bold font-size))

    (gamekit:draw-text (format nil "~A" *mouse-pos*)
                       (gamekit:add origin
                                    (gamekit:vec2 0 font-size))
                       :fill-color (grey-shade :value 1)
                       :font (gamekit:make-font :font-fira-mono-medium
                                                (/ font-size 2)))
    ;; draw mouse-state
    (gamekit:draw-text (format nil "m-state: ~A" *mouse-state*)
                       (gamekit:add origin
                                    (gamekit:vec2 0 (* font-size 2)))
                       :fill-color (grey-shade :value 1)
                       :font (gamekit:make-font :font-fira-mono-medium
                                                (/ font-size 2)))))

;;;------------------------------------------------------------
(defgeneric draw-button (button &key fill-paint stroke-paint))

(defmethod draw-button ((button button) &key (fill-paint (grey-shade :value .5))
                                              (stroke-paint (grey-shade :value 0)))
  "Generic button."
  (with-accessors ((ca press-area)
                   (w  width)
                   (h  height))
      button

    (let ((font-size (/ (height button) 5))
          (b-color (color button)))

      (case (state button)
        (:released
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint (gamekit:mult b-color 1)
                            :stroke-paint (gamekit:mult b-color 1.05)))
        (:hovered
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint (gamekit:mult b-color 1.1)
                            :stroke-paint (gamekit:mult b-color 1.15)))
        (:pressed
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint (gamekit:mult b-color 1.2)
                            :stroke-paint (gamekit:mult b-color 1.25)))
        (otherwise
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint fill-paint
                            :stroke-paint stroke-paint)))

      ;; draw state
      (gamekit:draw-text (format nil "b-state: ~A" (state button))
                         (gamekit:add (pos button)
                                      (gamekit:vec2 8 (+ 4 font-size)))
                         :fill-color (grey-shade :value 0)
                         :font (gamekit:make-font :font-fira-mono-medium
                                                  font-size))

      ;; draw signal-ready-p
      (gamekit:draw-text (format nil "signal-ready-p: ~A" (signal-ready-p button))
                         (gamekit:add (pos button)
                                      (gamekit:vec2 8 (+ 4 (* font-size 3))))
                         :fill-color (grey-shade :value 0)
                         :font (gamekit:make-font :font-fira-mono-medium
                                                  font-size))
      )))


(defmethod draw-button ((button mode-switch-button)
                        &key (fill-paint (grey-shade :value .5))
                          (stroke-paint (grey-shade :value 0)))
  "Generic button."
  (with-accessors ((ca press-area)
                   (w  width)
                   (h  height)
                   (c  color))
      button

    (let ((font-size (/ h 2))
          (b-color c)
          (text-x-offset (/ w 90)))

      (case (state button)
        (:released
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint (gamekit:mult b-color 1)
                            :stroke-paint (gamekit:mult b-color 1.05)))
        (:hovered
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint (gamekit:mult b-color 1.1)
                            :stroke-paint (gamekit:mult b-color 1.15)))
        (:pressed
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint (gamekit:mult b-color 1.2)
                            :stroke-paint (gamekit:mult b-color 1.25)))
        (otherwise
         (gamekit:draw-rect (car ca)
                            w h
                            :fill-paint fill-paint
                            :stroke-paint stroke-paint)))


      (gamekit:draw-text (format nil "~A" (switch-to-mode button))
                         (gamekit:add (pos button)
                                      (gamekit:vec2 text-x-offset (/ font-size 1.5)))
                         :fill-color (grey-shade :value 0)
                         :font (gamekit:make-font :font-fira-mono-medium
                                                  font-size))
      )))

;;;------------------------------------------------------------
(defun draw-buttons (b-list)
  "takes a list of buttons"
  (dolist (b b-list)
    (draw-button b :fill-paint (grey-shade :value .95))))

;;;------------------------------------------------------------
(defparameter *water-color* (gamekit:vec4 0.1 0.3 0.45 1))
(defparameter *land-color* (gamekit:vec4 0.4 0.6 0.3 1))


(defun at-least-one-component-1 (color)
  (or (not (= 1 (gamekit:x color)))
       (not (= 1 (gamekit:y color)))
       (not (= 1 (gamekit:z color)))))

(defun at-least-one-component-0 (color)
  (or (< (gamekit:x color) 0.1)
       (< (gamekit:y color) 0.1)
       (< (gamekit:z color) 0.1)))

;;;------------------------------------------------------------
(defun draw-map (map)
  (when (null map)
    (return-from draw-map))

  (let ;; ((x-times *h-tile-count*)  ; draw only part of map
       ;;  (y-times *v-tile-count*))
      ((x-times (basic-map-width map))
       (y-times (basic-map-height map)))
    (dotimes (i x-times)
      (dotimes (j y-times)
        (if (aref (basic-map-tiles map) i j)
            (draw-tiles (list (cons i j))
                        :fill-paint *land-color*
                        :stroke-paint *land-color*))))))
