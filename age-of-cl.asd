(asdf:defsystem #:age-of-cl
    :description "A top down RTS (real-time strategy game)."
    :author "decent-username decent@user.name"
    :license "GPLv3+"
    :version "0.0.1"
    :serial t
    :depends-on (trivial-gamekit)
    :components
    ((:module "src"
              :serial t
              :components
              ((:file "package")
               (:file "map-utils")
               (:file "ui-utils")
               (:file "draw-utils")
               (:file "age-of-cl")))))
